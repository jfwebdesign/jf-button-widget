=== JF Button Widget ===
Tags: page, category, category in page, tag in page, page archive, pages  
Requires at least: 3.3
Tested up to: 4.8
Stable tag: 1.0

This shortcode and widget allow you to but a fully styled button anywhere you want.

== Description ==
Creates a custom button for the sidebar. This shortcode and widget allow you to but a fully styled button anywhere you want.
For shortcode [jf_add_button title = "" link = ""  external_link = "" window = "" bgcolor = "" button_image ="" textcolor = "" rounded_left_top = "" rounded_right_top = "" rounded_left_bottom = "" rounded_right_bottom = "" padding_top = "" padding_right = "" padding_bottom = "" padding_left = "" text_align = "" width = "" button_align ="" button_hover =""].


== Changelog ==

= Version 1.0 =
Initial Plugin Release

== Installation ==

This section describes how to install the plugin and get it working.

1. Unzip archive and upload the entire folder to the /wp-content/plugins/ directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. It will do the rest.


== Frequently Asked Questions ==
Will be added as they come.
