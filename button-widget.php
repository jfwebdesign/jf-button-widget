<?php
/**
 * Plugin Name: JF Sidebar Button Widget and Button Shortcode
 * Plugin URI: https://www.jfwebdesign.com
 * Description: Creates a custom button for the sidebar. For shortcode [jf_add_button title = "" link = ""  external_link = "" window = "" bgcolor = "" button_image ="" textcolor = "" rounded_left_top = "" rounded_right_top = "" rounded_left_bottom = "" rounded_right_bottom = "" padding_top = "" padding_right = "" padding_bottom = "" padding_left = "" text_align = "" width = "" button_align ="" button_hover =""].
 * Version: 1.0.2
 * Author: JF WebDesign
 * Author URI: https://www.jfwebdesign.com
 */
$plugin_url = 'jf-button-widget/';

class jf_add_sidebar_button extends WP_Widget {


	function __construct() {
       parent::__construct(false, $name = __('Custom Sidebar Button', 'wp_widget_plugin') );
	}
	// widget form creation
	function form($instance) {	
	// Check values
		if( $instance) {
			 $title = esc_attr($instance['title']);
			 $link = esc_attr($instance['link']);
			 $external_link = esc_attr($instance['external_link']);
			 $window = esc_attr($instance['window']);
			 $bgcolor = esc_attr($instance['bgcolor']);
			 $button_image = esc_attr($instance['button_image']);
			 $textcolor = esc_attr($instance['textcolor']);
			 $rounded_left_top = esc_attr($instance['rounded_left_top']);
			 $rounded_right_top = esc_attr($instance['rounded_right_top']);
			 $rounded_left_bottom = esc_attr($instance['rounded_left_bottom']);
			 $rounded_right_bottom = esc_attr($instance['rounded_right_bottom']);
			 $padding_top = esc_attr($instance['padding_top']);
			 $padding_right = esc_attr($instance['padding_right']);
			 $padding_bottom = esc_attr($instance['padding_bottom']);
			 $padding_left = esc_attr($instance['padding_left']);
			 $text_align = 	esc_attr($instance['text_align']);
			 $button_align = 	esc_attr($instance['$button_align']);
			 $button_hover = 	esc_attr($instance['button_hover']);
			 $width = 	esc_attr($instance['width']);
			 } else {
			 $title = '';
			 $link = '';
			 $external_link = '';
			 $window = '_self';
			 $bgcolor = 'none';
			 $button_image = 'none';
			 $textcolor = '#000';
			 $rounded_left_top = '0px';
			 $rounded_right_top = '0px';
			 $rounded_left_bottom = '0px';
			 $rounded_right_bottom = '0px';
			 $padding_top = '0px';
			 $padding_right = '0px';
			 $padding_bottom = '0px';
			 $padding_left = '0px';
			 $text_align = 	'center';
			 $button_align = 'center';
			 $button_hover = '';
			 $width = 	'100%';
		}
		?>
            <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Button Text', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('Link URL:', 'wp_widget_plugin'); ?></label>
            <?php 
				$args = array(
					'depth'                 => 0,
					'child_of'              => 0,
					'selected' 				=> $instance['link'],
					'echo'                  => 1,
					'name'                  => $this->get_field_name('link'),
					//'id'                    => $this->get_field_id('link'), // string
					'class'                 => null, // string
					'show_option_none'      => 'Select a page', // string
					'show_option_no_change' => null, // string
					'option_none_value'     => null, // string
				); 
			?>
				<?php wp_dropdown_pages( $args ); ?> 
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('external_link'); ?>"><?php _e('External Link:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('external_link'); ?>" name="<?php echo $this->get_field_name('external_link'); ?>" type="text" value="<?php echo $external_link; ?>" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('window'); ?>"><?php _e('Window:', 'wp_widget_plugin'); ?></label>
            <select id="<?php echo $this->get_field_id('window'); ?>" name="<?php echo $this->get_field_name('window'); ?>">
            	<option value="_blank" <?php if ($instance['window'] == '_blank') {echo 'selected=selected';}?> >New Window</option>
                <option value="_self" <?php if ($instance['window'] != '_blank') {echo 'selected=selected';}?> >Same Window</option>
            </select>
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('bgcolor'); ?>"><?php _e('Background Color:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('bgcolor'); ?>" name="<?php echo $this->get_field_name('bgcolor'); ?>" type="text" value="<?php echo $bgcolor; ?>" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('button_hover'); ?>"><?php _e('Hover Effect:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('button_hover'); ?>" name="<?php echo $this->get_field_name('button_hover'); ?>" type="text" value="<?php echo $button_hover; ?>" placeholder="CSS Style" />
            <br>*Separate with semi-colon.
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('button_image'); ?>"><?php _e('Background Image URL:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('button_image'); ?>" name="<?php echo $this->get_field_name('button_image'); ?>" type="text" value="<?php echo $button_image; ?>" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('textcolor'); ?>"><?php _e('Text Color:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('textcolor'); ?>" name="<?php echo $this->get_field_name('textcolor'); ?>" type="text" value="<?php echo $textcolor; ?>" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('Button Width:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" type="text" value="<?php echo $width; ?>" placeholder="ex. 100% or 100px" />
            </p>
            
            <p>
            <label for="<?php echo $this->get_field_id('rounded_left_top'); ?>"><?php _e('Rounded Left Top:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('rounded_left_top'); ?>" name="<?php echo $this->get_field_name('rounded_left_top'); ?>" type="text" value="<?php echo $rounded_left_top; ?>" placeholder="0px" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('rounded_right_top'); ?>"><?php _e('Rounded Right Top:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('rounded_right_top'); ?>" name="<?php echo $this->get_field_name('rounded_right_top'); ?>" type="text" value="<?php echo $rounded_right_top; ?>" placeholder="0px" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('rounded_left_bottom'); ?>"><?php _e('Rounded Left Bottom:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('rounded_left_bottom'); ?>" name="<?php echo $this->get_field_name('rounded_left_bottom'); ?>" type="text" value="<?php echo $rounded_left_bottom; ?>" placeholder="0px" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('rounded_right_bottom'); ?>"><?php _e('Rounded Right Bottom:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('rounded_right_bottom'); ?>" name="<?php echo $this->get_field_name('rounded_right_bottom'); ?>" type="text" value="<?php echo $rounded_right_bottom; ?>" placeholder="0px" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('padding_top'); ?>"><?php _e('Top Padding:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('padding_top'); ?>" name="<?php echo $this->get_field_name('padding_top'); ?>" type="text" value="<?php echo $padding_top; ?>" placeholder="0px" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('padding_right'); ?>"><?php _e('Right Padding:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('padding_right'); ?>" name="<?php echo $this->get_field_name('padding_right'); ?>" type="text" value="<?php echo $padding_right; ?>" placeholder="0px" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('padding_bottom'); ?>"><?php _e('Bottom Padding:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('padding_bottom'); ?>" name="<?php echo $this->get_field_name('padding_bottom'); ?>" type="text" value="<?php echo $padding_bottom; ?>" placeholder="0px" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('padding_left'); ?>"><?php _e('Left Padding:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('padding_left'); ?>" name="<?php echo $this->get_field_name('padding_left'); ?>" type="text" value="<?php echo $padding_left; ?>" placeholder="0px" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('text_align'); ?>"><?php _e('Text Align:', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('text_align'); ?>" name="<?php echo $this->get_field_name('text_align'); ?>" type="text" value="<?php echo $text_align; ?>" placeholder="left center right justify" />
            </p>
            <p>
            <label for="<?php echo $this->get_field_id('button_align'); ?>"><?php _e('Button Align:', 'wp_widget_plugin'); ?></label>
            <select id="<?php echo $this->get_field_id('button_align'); ?>" name="<?php echo $this->get_field_name('button_align'); ?>">
            	<option value="left" <?php if ($instance['button_align'] == 'left') {echo 'selected=selected';}?> >Left</option>
                <option value="center" <?php if ($instance['button_align'] == 'center') {echo 'selected=selected';}?> >Center</option>
                <option value="right" <?php if ($instance['button_align'] == 'right') {echo 'selected=selected';}?> >Right</option>
            </select>
            </p>
        <?
	}

	// widget update
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		// Fields
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['link'] = strip_tags($new_instance['link']);
		$instance['external_link'] = strip_tags($new_instance['external_link']);
		$instance['window'] = strip_tags($new_instance['window']);
		$instance['bgcolor'] = strip_tags($new_instance['bgcolor']);
		$instance['button_image'] = strip_tags($new_instance['button_image']);
		$instance['textcolor'] = strip_tags($new_instance['textcolor']);
		$instance['rounded_left_top'] = strip_tags($new_instance['rounded_left_top']);
		$instance['rounded_right_top'] = strip_tags($new_instance['rounded_right_top']);
		$instance['rounded_left_bottom'] = strip_tags($new_instance['rounded_left_bottom']);
		$instance['rounded_right_bottom'] = strip_tags($new_instance['rounded_right_bottom']);
		$instance['padding_top'] = strip_tags($new_instance['padding_top']);
		$instance['padding_right'] = strip_tags($new_instance['padding_right']);
		$instance['padding_bottom'] = strip_tags($new_instance['padding_bottom']);
		$instance['padding_left'] = strip_tags($new_instance['padding_left']);
		$instance['text_align'] = strip_tags($new_instance['text_align']);
		$instance['button_align'] = strip_tags($new_instance['button_align']);
		$instance['button_hover'] = strip_tags($new_instance['button_hover']);
		$instance['width'] = strip_tags($new_instance['width']);
		return $instance;	
	 }

	// widget display
	function widget($args, $instance) {
	extract( $args );
   // these are the widget options
	$title = apply_filters('widget_title', $instance['title']);
	$link =  get_permalink( $instance['link'] );
	$external_link = $instance['external_link'];
	$window = $instance['window'];
	$bgcolor = $instance['bgcolor'];
	$button_image = $instance['button_image'];
	$textcolor = $instance['textcolor'];
	$rounded_left_top = $instance['rounded_left_top'];
	$rounded_right_top = $instance['rounded_right_top'];
	$rounded_left_bottom = $instance['rounded_left_bottom'];
	$rounded_right_bottom = $instance['rounded_right_bottom'];
	$padding_top = $instance['padding_top'];
	$padding_right = $instance['padding_right'];
	$padding_bottom = $instance['padding_bottom'];
	$padding_left = $instance['padding_left'];
	$text_align = $instance['text_align'];
	$button_align = $instance['button_align'];
	$button_hover = $instance['button_hover'];
	$width = $instance['width'];
	$margin = '';
		if ($button_align == "center") {$margin = "margin: 0 auto; ";}
		if ($button_align == "right") {$margin = "float: right; ";}

		if ($button_image == 'none') 
		{
			$button_image = '';
		}
		if ($button_image != '') 
		{
			$bgcolor = '';
		}
		$style = '<style>#'.$args['widget_id'].' .widget-button {color:'.$textcolor. '; text-align: '.$text_align.'; '.$margin.'background-color:'.$bgcolor. '; border-radius:' .$rounded_left_top. ' ' . $rounded_right_top. ' ' . $rounded_right_bottom . ' ' . $rounded_left_bottom. '; padding:' .$padding_top. ' ' . $padding_right. ' ' . $padding_bottom . ' ' . $padding_left. '; width: '.$width.';} #'.$args['widget_id'].' .widget-button:hover {'.$button_hover.'}</style>';	
	if ($external_link == '') 
	{$before_title =  '<a href="'.$link .'" target="'.$window.'" >';}
	else 
	{$before_title =  '<a href="'.$external_link .'" target="'.$window.'" >';}
	$after_title = '</a>';
		echo $style;
		echo $before_widget;
		// Display the widget
		echo '<div class="widget-button-wrap" style="width: 100%;">';
		$button_wrap = '<div class="widget-button" >';
		$end_button_wrap = '</div>';
		if ($button_image != '') 
		{
			echo $before_title .'<img src="'.$button_image.'">' . $after_title;
		}
		else {
		   // Check if title is set
		   if ( $title) {
			  echo $before_title .$button_wrap. $title .$end_button_wrap . $after_title;
		   } 
		}
		echo '</div>';
		echo $after_widget;
	}
}

// register widget
// depricated: add_action('widgets_init', create_function('', 'return register_widget("jf_add_sidebar_button");'));
	add_action ( 'widgets_init', 'init_jf_add_sidebar_button' );function init_jf_add_sidebar_button() {return register_widget('jf_add_sidebar_button');}
//add button in shortcode

add_shortcode('jf_add_button', 'load_jf_button'); // [jf_add_button]

function load_jf_button($atts)
{
	// Attributes
	extract( shortcode_atts(
		array(
			$title => '',
			$link => '',
			$external_link => '',
			$window => '_self',
			$bgcolor => 'none',
			$button_image => 'none',
			$textcolor => '#000',
			$rounded_left_top => '0px',
			$rounded_right_top => '0px',
			$rounded_left_bottom => '0px',
			$rounded_right_bottom => '0px',
			$padding_top => '0px',
			$padding_right => '0px',
			$padding_bottom => '0px',
			$padding_left => '0px',
			$text_align => 	'center',
			$button_align => 	'center',
			$button_hover => '',
			$width => 	'100%'
			
		), $atts )
	);
$attributes = array($atts); 
foreach ($attributes as $value) {
	$title = $value['title'];
	$link =  $value['link'];
	$external_link = $value['external_link'];
	$window = $value['window'];
	$bgcolor = $value['bgcolor'];
	$button_image = $value['button_image'];
	$textcolor = $value['textcolor'];
	$rounded_left_top = $value['rounded_left_top'];
	$rounded_right_top = $value['rounded_right_top'];
	$rounded_left_bottom = $value['rounded_left_bottom'];
	$rounded_right_bottom = $value['rounded_right_bottom'];
	$padding_top = $value['padding_top'];
	$padding_right = $value['padding_right'];
	$padding_bottom = $value['padding_bottom'];
	$padding_left = $value['padding_left'];
	$text_align = 	$value['text_align'];
	$button_align = 	$value['button_align'];
	$button_hover = 	$value['button_hover'];
	$width = 	$value['width'];
	$random_id = "jf_button_".rand(1234, 999999);
	$margin = '';
	if ($button_image == 'none') 
		{
			$button_image = '';
		}
	if ($button_image != '') 
		{
			$bgcolor = '';
		}
	if ($button_align == "center") {$margin = "margin: 0 auto; ";}
	if ($button_align == "right") {$margin = "float: right; ";}
	$style = '<style>#'.$random_id.' .widget-button {color:'.$textcolor. '; text-align: '.$text_align.'; '.$margin.'background-color:'.$bgcolor. '; border-radius:' .$rounded_left_top. ' ' . $rounded_right_top. ' ' . $rounded_right_bottom . ' ' . $rounded_left_bottom. '; padding:' .$padding_top. ' ' . $padding_right. ' ' . $padding_bottom . ' ' . $padding_left. '; width: '.$width.';} #'.$random_id.' .widget-button:hover {'.$button_hover.'}</style>';	
}
ob_start();
if ($external_link == '') 
	{$before_title =  '<a href="'.$link .'" target="'.$window.'" >';}
	else 
	{$before_title =  '<a href="'.$external_link .'" target="'.$window.'" >';}
	$after_title = '</a>';
		echo $style;
		// Display the widget
		echo '<div id="'.$random_id.'" class="widget-button-wrap" style="width: 100%;">';
		$button_wrap = '<div class="widget-button">';
		$end_button_wrap = '</div>';
		if ($button_image != '') 
		{
			echo $before_title .'<img src="'.$button_image.'">' . $after_title;
		}
		else {
		   // Check if title is set
		   if ( $title) {
			  echo $before_title .$button_wrap. $title .$end_button_wrap . $after_title;
		   } 
		}
		echo '</div>';
   return ob_get_clean();
}
?>